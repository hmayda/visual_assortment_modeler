{ nixpkgs ? <nixpkgs>
, platform_release ? "4.18.latest"
, src_modeler_app ? ./.
, doCheck ? true
, pkgs ? import nixpkgs{}
}:
let
  builder_config = import <config> { inherit nixpkgs; };
in
  builder_config.genericAppJobset {
    logicblox = builder_config.getLB platform_release;

    build =
      builder_config.buildModelerApp {
        name = "modeler-now";
        src = src_modeler_app;
        inherit doCheck;
        buildInputs =  [ pkgs.python35 ];
      };
  }
