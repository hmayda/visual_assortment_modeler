#! /usr/bin/env python

from lbconfig.api import *
from lbconfig import modeler

import os

#######################################################################
# 1- Configuration
#

app_name='modeler-now'
core_library=app_name
workflow_library="%s-wf" % app_name
basedir=os.path.dirname(os.path.realpath(__file__))

lbconfig_package(app_name,
                 version='0.9',
                 default_targets=['lb-libraries',
                                  'warmup-msg',
                                  'workflow-doc'                                  
                     ])

depends_on(logicblox_dep,
           lb_web_dep,
           lb_measure_service='$(LB_MEASURE_SERVICE_HOME)',
           modelerjs='$(LOGICBLOX_HOME)',
           lb_workflow='$(LB_WORKFLOW_HOME)',           
           lb_web_workbooks='$(LB_WORKBOOK_SERVICE_HOME)')

variable('modelerjs_py','$(LOGICBLOX_HOME)/python')

#######################################################################
# 2- LogiQl Libraries/Projects
#

libraries = [
  core_library
]

for lib in libraries:
    lb_library(
       name = lib,
       srcdir = '%s/src/logiql' % basedir,
       generated = False,
       deps = { 'lb_web': '$(lb_web)',
                'lb_web_credentials': '$(lb_web)',
                'lb_web_connectblox': '$(lb_web)',
                'lb_web_workbooks': '$(lb_web_workbooks)',
                'lb_measure_service': '$(lb_measure_service)',
                'pivot': '$(modelerjs)',
                'modeler_platform': '$(modelerjs)',
                'modeler_init': '$(modelerjs)',
                'modeler_config': '$(modelerjs)',                
                'permissions': '$(modelerjs)'})

modeler.modeler_library(name='generated_schema',
                        config_dir='src/config',
                        python_dir='$(modelerjs_py)',
                        include_permissions=False)

modeler.configure(app_name,
                  app_base_dir=basedir,
                  libraries=['generated_schema'],
                  js_scripts_dest_dir='%s/scripts/js' % basedir,
                  js_lib_dest_dir='%s/node_modules' % basedir)                    

rule(output='warmup-msg',
     input='$(build)/srcgen/warmup_msg/measure_rules.json',
     phony=True)

rule(output='$(build)/srcgen/warmup_msg/measure_rules.json',
     input=modeler._csv_config_files('%s/src/config' % basedir),
     commands=['python $(modelerjs_py)/generate_warmup_from_config.py ' +
               '%s/src/config $(build)/srcgen/warmup_msg' % basedir])

#######################################################################
# 3- Workspaces
#

run_ws='/%s' % app_name
workflow_ws = '/workflow'
workbook_base_branch = '__workbook_base_branch'

ws_archive(name=run_ws,
           libraries=[core_library],
           compress=False,
           keep=True)

rule(output='$(build)/.workflow-workspace',
     input=[],
     commands=['lb workflow create --overwrite',
               'touch build/.workflow-workspace'])

rule(
    output=archive_export_target(run_ws),
    input=['$(build)/build-workspace.success']
)

rule(
    output='$(build)/build-workspace.success',
    input=['$(build)/.load-handlers',
           check_ws_success_file(run_ws),
           '$(build)/.workflow-workspace'],
    commands=[
        'bash %s/scripts/build_load_meta_model.sh %s' % (basedir, run_ws),
        'python $(modelerjs_py)/install_ui.py %s' % app_name,
        'bash %s/scripts/build_from_rules.sh %s %s %s' % (basedir, app_name, run_ws, workbook_base_branch),
        'touch $(build)/build-workspace.success'])

rule(output='all',
     input=['workflow-doc',
            '$(build)/build-workspace.success',
            '$(build)/workspaces/workflow.tgz',
            archive_export_target(workflow_ws)],
     phony=True)

rule(output='$(build)/workspaces/%s.tgz' % app_name,
     input=['archive-ws-%s' % run_ws],
     commands=['cd $(build)/workspaces && tar cfz %s.tgz %s/' % (app_name, app_name)])

rule(output='$(build)/workspaces/workflow.tgz',
     input=[archive_export_target(workflow_ws)],
     commands=['cd $(build)/workspaces && tar cfz workflow.tgz workflow/'])

#######################################################################
# 4- Install
#

install_file('$(build)/workspaces/%s.tgz' % app_name, 'workspaces')
install_file('$(build)/workspaces/workflow.tgz', 'workspaces')
install_dir('$(modelerjs)/share/modeler/pivot', 'share') 
install_dir('$(modelerjs)/share/modeler/modeler_config', 'share/modeler_config')
install_file('$(modelerjs_py)/install_ui.py', 'scripts')
install_dir('$(build)/cleancsvs', 'build/cleancsvs')
install_dir('$(build)/doc', 'build/doc')
install_dir('$(build)/srcgen/warmup_msg', 'build/srcgen/warmup_msg')
install_dir('frontend', 'static')
install_dir('%s/scripts/batch' % basedir, 'scripts/batch')
install_dir('%s/src/config' % basedir, 'src/config')
install_dir('src/rules', 'src/rules')
install_dir('%s/src/workflows' % basedir, 'src/workflows')
install_file('%s/install.sh' % basedir, '')

#######################################################################
# 5- Deploy (build workspaces)
#

def refresh_services(ws):
    return [
    'lb web-server unload-services -w ' + run_ws,
    'lb web-server load-services -w ' + run_ws
    ]

def load_ui():
    return ['python $(modelerjs_py)/install_ui.py %s' % app_name]

rule(output='$(build)/.$(build)/.refresh-services',
     input=[check_ws_success_file(run_ws), '$(build)/.load-handlers'],
     commands=refresh_services(run_ws))

rule(output='$(build)/.load-handlers',
     input=[],
     commands=['lb web-server load --jar $(lb_web)/lib/java/handlers/bloxweb-connectblox.jar',
               'lb web-server load --jar $(lb_measure_service)/lib/java/handlers/lb-measure-service.jar',
               'lb web-server load --jar $(lb_web_workbooks)/lib/java/lb-web-workbooks.jar',
               'lb web-server load --jar $(lb_web)/lib/java/handlers/lb-web-json.jar',
               'touch $(build)/.load-handlers'])

rule('start-services-app', [check_ws_success_file(run_ws), '$(build)/.load-handlers'], [
    'lb web-server load-services -w ' + run_ws],
    phony=True)


def load_config(ws): 
    return [
        'lb web-client batch -i %s/scripts/batch/config.batch' % basedir,
        'lb execblock ' + ws + ' modeler_config:metamodel:translate']

def refresh_measure_model(ws,cleandir):
    return [
        # delete existing model
        'echo \'{}\' | lb web-client call http://localhost:55183/%s/refresh-metamodel ' % app_name,
        # clean csv files
        'python $(modelerjs_py)/clean_csvs.py src/config ' + cleandir,
        # reload model
        'lb web-client batch -i %s/scripts/batch/config.batch' % basedir,
        'lb execblock ' + ws + ' modeler_config:metamodel:translate',
        # refresh measure service
        'lb exec ' + ws + ' \'^measure:admin:allowed[]="enabled".\'',
        'lb measure-service admin refresh --uri http://localhost:55183/%s/measure' % app_name]

rule('load-config', [], load_config(run_ws), phony=True)

rule('load-ui', '', load_ui(), phony=True)

rule('load-cubiql-rules', '',
     ['lb replace-default-branch %s before-load-cubiql-rules' % run_ws,
      'bash %s/scripts/build_from_rules.sh %s %s %s' % (basedir, app_name, run_ws, workbook_base_branch)],
     phony=True)

def dev_data_root():
    return 'file://' + basedir + '/data/dev_data'

def perf_data_root():
    return 'file://' + basedir + '/data/perf_dev_data'

def dev_constant_root():
    return dev_data_root() + "/constants"

def dev_initial_data_root():
    return dev_data_root() + "/initial"

def perf_constant_root():
    return perf_data_root() + "/constants"

def perf_initial_data_root():
    return perf_data_root() + "/initial"

def dev_daily_data_root():
    return 'file://' + basedir + '/data/daily_data'

rule('load-test-data',
     input=['build/.workflow-workspace'],
     commands=[
         'lb replace-default-branch %s before-data-load' % run_ws,
         'lb workflow run -q --file %s/src/workflows/dev.wf --main dev.deploy_initial_data --param constant_location=%s --param location=%s --param timeout=1440000s --max-retries 2' % ( basedir, dev_constant_root(), dev_initial_data_root() )
         ],
    phony=True)

rule('refresh-measure-service', '',
     commands=[
        'lb exec %s \'^measure:admin:allowed[]="enabled".\'' % run_ws,
        'lb measure-service admin refresh --uri http://localhost:55183/%s/measure' % app_name],
    phony=True)

rule('build-all-workbooks', 
     input=['build/.workflow-workspace'],
     commands=[
         'lb workflow run -q --file %s/src/workflows/workbooks.wf --main workbooks.build_all_workbooks --max-retries 2' % basedir ],
     phony=True)

rule('daily-batch', 
     input=['build/.workflow-workspace'],
     commands=[
         'lb workflow run -q --file %s/src/workflows/main/daily.wf --main daily_batch --param location=%s --param timeout=1440000s --max-retries 2' % (basedir, dev_daily_data_root() )
         ],
    phony=True)

rule('remove-user-preferences',[],
     commands= [
      'lb exec ' + run_ws + ' \' -modeler_platform:views:model:user_viewstate[s,user,template] = _ <- modeler_platform:views:model:user_viewstate@prev[s,user,template] = _.\'',
      ])

rule(output='refresh-measure-model',
     input=modeler._csv_config_files('%s/src/config' % basedir),
     commands=refresh_measure_model(run_ws,'$(build)/cleancsvs'))

rule(output='add-metrics',
     input=[],
     commands=[
         'python $(modelerjs_py)/add_metric.py %s %s' % ('src/config', run_ws),
     ] + load_config(run_ws) +refresh_services(run_ws))

rule('start-nginx', [], ['sudo nginx -g "user %s staff;" -p $(PWD)/ -c conf/nginx.conf | grep -v "could not open error log file"' % os.getenv("USER")])

#######################################################################
# 7- Clean
#

rule(output='clean',
     input=[],
     commands=['rm -rf build',
               'rm -rf out',
               'rm -f %s/error' % dev_data_root(),
               'lb web-server unload-services -w %s || echo No %s workspace' % (run_ws, run_ws),
               'lb web-server unload-services -w %s || echo No %s workspace' % (workflow_ws, workflow_ws)])

#######################################################################
# 8- Workflow
#

lb_library(name=workflow_library,
           srcdir='%s/src/logiql' % basedir,
           generated=False,
           deps={'lb_web': '$(lb_web)',
                 'lb_workflow': '$(lb_workflow)'})

ws_archive(name=workflow_ws, libraries=[workflow_library], compress=False, keep=True)

rule(
     output=archive_export_target(workflow_ws),
     input=['$(build)/build-wf-workspace.success'])

rule(
     output='$(build)/build-wf-workspace.success',
     input=[check_ws_success_file('/workflow')],
     commands=[
         'bash %s/scripts/build_wf_workspace.sh' % basedir,
         'touch $(build)/build-wf-workspace.success'])

rule(output=workflow_library,
     input=['check-ws-%s' % workflow_library],
     commands=['lb web-server load-services -w %s' % workflow_ws,
               '%s/scripts/install_workflows.sh .' % basedir])

lb_workflow_doc('%s/src/workflows/readme.wf' % basedir, title='%s Workflow Documentation' % app_name)

#######
# 9 - tests
#
# TODO: test_batches.sh

rule(output='check',
     input=[],
     commands=[
         'bash scripts/setup_tests.sh %s' % run_ws,
         'echo running tests',
         'bash tests/smoke/load.sh %s smoketest_master_dev' % run_ws])
