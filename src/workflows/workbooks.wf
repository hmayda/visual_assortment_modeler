
/**
 * Loop through all .json files in src/config/workbooks/templates, to create workbook templates
 */
workflow workbooks.create_templates(app_prefix, src_root) [ template_files* ] {
  ($template_files = matches) <~ lb.ListFiles(patterns = { "$(src_root)/src/config/workbooks/templates/*.json" })
  ;
  forall( template in $template_files ) {
    workbooks.create_template($app_prefix, $template)
  }
}

workflow workbooks.create_template( app_prefix, template ) 
  lb.wb.CreateTemplate(
    app_prefix = $app_prefix,
    json_spec_file = $template)

workflow workbooks.create_partitioned_by_class_wb(app_prefix="modeler-now") [template_instantiation* ] {
	($template_instantiation) <~ lb.wb.util.GetTemplateInstantiationRefByTemplate(
       app_prefix = $app_prefix,
       template_name = "partitioned_by_class")
     ;
     forall<max=3>(ti in $template_instantiation)
       lb.wb.util.CreateWorkbook(
         app_prefix = $app_prefix,
         template_instantiation = $ti) 
 }

 workflow workbooks.create_partitioned_by_class_region_wb(app_prefix="modeler-now") [template_instantiation* ] {
	 ($template_instantiation) <~ lb.wb.util.GetTemplateInstantiationRefByTemplate(
       app_prefix = $app_prefix,
       template_name = "partitioned_by_class_region")
     ;
     forall<max=3>(ti in $template_instantiation)
       lb.wb.util.CreateWorkbook(
         app_prefix = $app_prefix,
         template_instantiation = $ti)
}

workflow workbooks.create_unpartitioned_wb(app_prefix="modeler-now") [template_instantiation* ] {
	 ($template_instantiation) <~ lb.wb.util.GetTemplateInstantiationRefByTemplate(
       app_prefix = $app_prefix,
       template_name = "unpartitioned")
     ;
     forall<max=3>(ti in $template_instantiation)
       lb.wb.util.CreateWorkbook(
         app_prefix = $app_prefix,
         template_instantiation = $ti)
}

workflow workbooks.build_all_workbooks(app_prefix = "modeler-now", timeout="86400000s") {
  workbooks.create_partitioned_by_class_wb(app_prefix = $app_prefix)
  ||
  workbooks.create_partitioned_by_class_region_wb(app_prefix = $app_prefix)
  ||
  workbooks.create_unpartitioned_wb(app_prefix = $app_prefix)
}
