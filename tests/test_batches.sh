#! /usr/bin/env bash

set -e

WS=$1

# first load test data
lb replace-default-branch $WS before-data-load
lb workflow run --auto-terminate 1 --overwrite --file src/workflows/dev.wf --main dev.deploy_initial_data --param constant_location=data/dev_data/constants --param location=data/dev_data/initial/blade --param timeout=1440000 --max-retries 2

# run build workbooks
lb workflow run --auto-terminate 1 --overwrite --file src/workflows/workbooks.wf --main workbooks.build_all_workbooks --max-retries 2
