import os
import sys

if not 'LOGICBLOX_HOME' in os.environ:
   print("error: LOGICBLOX_HOME environment variable is not defined")
   sys.exit(1)

lbpath =  os.path.join(os.environ.get('LOGICBLOX_HOME'), 'lib', 'python')
sys.path.insert(0, lbpath)

from dbcommands import lb_command
import blox.connect.io

class Session(object):
   
   def __init__(self, workspace):
      self.workspace = workspace
      self.conn = blox.connect.io.Connection(False)
      self.conn.open()

   def close(self):
      self.conn.close()

   def popcount(self, predname):
      cmd = lb_command.PredicatePopcountCommand(
          conn = self.conn,
          workspace = self.workspace,
          predicate = [predname],
          cwd = None)
      response = cmd.run()
      return response.pred_popcount.popcount[0].popcount

   def exec_logic(self, execlogic):
      cmd = lb_command.QueryLogicCommand(
         conn = self.conn,
         workspace = self.workspace,
         logic = execlogic,
         escape = True )
      response = cmd.run()
      return response
