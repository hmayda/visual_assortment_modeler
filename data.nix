{ fetchs3 }:
{
  dev_data =
    fetchs3 {
      url = s3://logicblox-private/data/modeler-now/dev_data.v1.20151104.tar.gz;
      sha256 = "233116e91867e272fbb253dd157798eadd2efff1e6dac3be6649cb171fd626e0";
    };
}