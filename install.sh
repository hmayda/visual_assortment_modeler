#! /usr/bin/env bash

set -e
set -x

function inflate_and_import {
    # assume $2.tgz exists in dir $1
    # inflate tgz to a safe place, a created dir in LB_DEPLOYMENT_HOME
    # needs to exist locally, on Hydra, and on deployed servers
    # finally, import the inflated archive and remove the archive
    dir=$1
    ws=$2
    mkdir -p "${LB_DEPLOYMENT_HOME}/inflated"
    rm -rf "${LB_DEPLOYMENT_HOME}/inflated/${ws}"
    tar xfz "${dir}/${ws}.tgz" -C "${LB_DEPLOYMENT_HOME}/inflated"
    lb import-workspace --overwrite "${ws}" "${LB_DEPLOYMENT_HOME}/inflated/${ws}"
    rm -r "${LB_DEPLOYMENT_HOME}/inflated/${ws}"

    lb web-server load-services -w "${ws}"
}

deploy_only="no"
initialize_data="no"
data_root=""
constant_root=""

for i in "$@"
do
case $i in
    --deploy-only)
    deploy_only="yes"
    shift
    ;;
    --initialize-data)
    initialize_data="yes"
    shift
    ;;
    --data-root=*)
    data_root="${i#*=}"
    shift
    ;;
    --constant-root=*)
    constant_root="${i#*=}"
    shift
    ;;
    *)
    # unknown option
    ;;
esac
done

echo "deploy_only     = ${deploy_only}"
echo "initialize_data = ${initialize_data}" 

if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 $1
fi

if [ "${initialize_data}" == "yes" ]; then
    if [[ "${data_root}" == ""  || "${constant_root}" == "" ]]; then
	echo "If initialize-data option is specified, data-root and constant-root options must be specified as well"
	exit 1
    fi
fi

if [ "${deploy_only}" == "yes" ]; then
    echo "Option -z set; deploy only, no install"
    exit 0
fi

if [ `uname` == "Darwin" ]; then
    script_dir=$(cd "$(dirname "$0")"; pwd)
else
    script_dir=$(dirname $(readlink -f $0))
fi
base_dir=$script_dir

#
# Master workspace
#

inflate_and_import workspaces /modeler-now

#
# Workflow workspace
#

inflate_and_import workspaces /workflow

#
# END
#

if [ "${initialize_data}" == "yes" ]; then
   echo "Option -b set; loading bootstrap data"
   lb exec /workflow "^workflow:config:app_prefix[] = \"modeler-now\". ^workflow:config:data_root[] = \"${data_root}\". ^workflow:config:constant_root[] = \"${constant_root}\".  ^workflow:config:timeout[] = \"86400000\"."
   lb workflow start --name deploy_initial
fi

exit 0
