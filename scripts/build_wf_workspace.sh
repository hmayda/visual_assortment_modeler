#!/usr/bin/env bash

set -e
set -x

if [ `uname` == "Darwin" ]; then
    script_dir=$(cd "$(dirname "$0")"; pwd)
else
    script_dir=$(dirname $(readlink -f $0))
fi
base_dir=$script_dir

echo "script_dir: $script_dir"

WS=/workflow

# load services
echo "Load services"
lb web-server load-services -w $WS

echo "Installing workflows"
$script_dir/install_workflows.sh src
